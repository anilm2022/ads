CREATE TABLE actor  ( 
    actor_id   	smallint UNSIGNED AUTO_INCREMENT NOT NULL,
    first_name 	varchar(45) NOT NULL,
    last_name  	varchar(45) NOT NULL,
    last_update	timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(actor_id) USING BTREE
)
ENGINE = InnoDB
CHARACTER SET utf8mb3
COLLATE utf8_general_ci
AUTO_INCREMENT = 200
GO
CREATE INDEX idx_actor_last_name USING BTREE 
    ON actor(last_name)
GO
CREATE TABLE address  ( 
    address_id 	smallint UNSIGNED AUTO_INCREMENT NOT NULL,
    address    	varchar(50) NOT NULL,
    address2   	varchar(50) NULL,
    district   	varchar(20) NOT NULL,
    city_id    	smallint UNSIGNED NOT NULL,
    postal_code	varchar(10) NULL,
    phone      	varchar(20) NOT NULL,
    location   	geometry NOT NULL,
    last_update	timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(address_id) USING BTREE
)
ENGINE = InnoDB
CHARACTER SET utf8mb3
COLLATE utf8_general_ci
AUTO_INCREMENT = 605
GO
ALTER TABLE address
    ADD CONSTRAINT fk_address_city
	FOREIGN KEY(city_id)
	REFERENCES city(city_id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
GO
CREATE INDEX idx_fk_city_id USING BTREE 
    ON address(city_id)
GO
CREATE SPATIAL INDEX idx_location
    ON address(location)
GO
CREATE TABLE category  ( 
    category_id	tinyint UNSIGNED AUTO_INCREMENT NOT NULL,
    name       	varchar(25) NOT NULL,
    last_update	timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(category_id) USING BTREE
)
ENGINE = InnoDB
CHARACTER SET utf8mb3
COLLATE utf8_general_ci
AUTO_INCREMENT = 16
GO
CREATE TABLE city  ( 
    city_id    	smallint UNSIGNED AUTO_INCREMENT NOT NULL,
    city       	varchar(50) NOT NULL,
    country_id 	smallint UNSIGNED NOT NULL,
    last_update	timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(city_id) USING BTREE
)
ENGINE = InnoDB
CHARACTER SET utf8mb3
COLLATE utf8_general_ci
AUTO_INCREMENT = 600
GO
ALTER TABLE city
    ADD CONSTRAINT fk_city_country
	FOREIGN KEY(country_id)
	REFERENCES country(country_id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
GO
CREATE INDEX idx_fk_country_id USING BTREE 
    ON city(country_id)
GO
CREATE TABLE country  ( 
    country_id 	smallint UNSIGNED AUTO_INCREMENT NOT NULL,
    country    	varchar(50) NOT NULL,
    last_update	timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(country_id) USING BTREE
)
ENGINE = InnoDB
CHARACTER SET utf8mb3
COLLATE utf8_general_ci
AUTO_INCREMENT = 109
GO
CREATE TABLE customer  ( 
    customer_id	smallint UNSIGNED AUTO_INCREMENT NOT NULL,
    store_id   	tinyint UNSIGNED NOT NULL,
    first_name 	varchar(45) NOT NULL,
    last_name  	varchar(45) NOT NULL,
    email      	varchar(50) NULL,
    address_id 	smallint UNSIGNED NOT NULL,
    active     	tinyint(1) NOT NULL DEFAULT '1',
    create_date	datetime NOT NULL,
    last_update	timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(customer_id) USING BTREE
)
ENGINE = InnoDB
CHARACTER SET utf8mb3
COLLATE utf8_general_ci
AUTO_INCREMENT = 599
GO
ALTER TABLE customer
    ADD CONSTRAINT fk_customer_store
	FOREIGN KEY(store_id)
	REFERENCES store(store_id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
GO
ALTER TABLE customer
    ADD CONSTRAINT fk_customer_address
	FOREIGN KEY(address_id)
	REFERENCES address(address_id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
GO
CREATE TRIGGER customer_create_date 
    BEFORE INSERT ON customer
    FOR EACH ROW
    SET NEW.create_date = NOW()
GO
CREATE INDEX idx_fk_address_id USING BTREE 
    ON customer(address_id)
GO
CREATE INDEX idx_fk_store_id USING BTREE 
    ON customer(store_id)
GO
CREATE INDEX idx_last_name USING BTREE 
    ON customer(last_name)
GO
CREATE TABLE film_actor  ( 
    actor_id   	smallint UNSIGNED NOT NULL,
    film_id    	smallint UNSIGNED NOT NULL,
    last_update	timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(actor_id,film_id) USING BTREE
)
ENGINE = InnoDB
CHARACTER SET utf8mb3
COLLATE utf8_general_ci
AUTO_INCREMENT = 0
GO
ALTER TABLE film_actor
    ADD CONSTRAINT fk_film_actor_film
	FOREIGN KEY(film_id)
	REFERENCES film(film_id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
GO
ALTER TABLE film_actor
    ADD CONSTRAINT fk_film_actor_actor
	FOREIGN KEY(actor_id)
	REFERENCES actor(actor_id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
GO
CREATE INDEX idx_fk_film_id USING BTREE 
    ON film_actor(film_id)
GO
CREATE TABLE film_category  ( 
    film_id    	smallint UNSIGNED NOT NULL,
    category_id	tinyint UNSIGNED NOT NULL,
    last_update	timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(film_id,category_id) USING BTREE
)
ENGINE = InnoDB
CHARACTER SET utf8mb3
COLLATE utf8_general_ci
AUTO_INCREMENT = 0
GO
ALTER TABLE film_category
    ADD CONSTRAINT fk_film_category_film
	FOREIGN KEY(film_id)
	REFERENCES film(film_id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
GO
ALTER TABLE film_category
    ADD CONSTRAINT fk_film_category_category
	FOREIGN KEY(category_id)
	REFERENCES category(category_id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
GO
CREATE INDEX fk_film_category_category USING BTREE 
    ON film_category(category_id)
GO
CREATE TABLE film_text  ( 
    film_id    	smallint NOT NULL,
    title      	varchar(255) NOT NULL,
    description	text NULL,
    PRIMARY KEY(film_id) USING BTREE
)
ENGINE = InnoDB
CHARACTER SET utf8mb3
COLLATE utf8_general_ci
AUTO_INCREMENT = 0
GO
CREATE FULLTEXT INDEX idx_title_description
    ON film_text(title, description)
GO
CREATE TABLE inventory  ( 
    inventory_id	mediumint UNSIGNED AUTO_INCREMENT NOT NULL,
    film_id     	smallint UNSIGNED NOT NULL,
    store_id    	tinyint UNSIGNED NOT NULL,
    last_update 	timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(inventory_id) USING BTREE
)
ENGINE = InnoDB
CHARACTER SET utf8mb3
COLLATE utf8_general_ci
AUTO_INCREMENT = 4581
GO
ALTER TABLE inventory
    ADD CONSTRAINT fk_inventory_store
	FOREIGN KEY(store_id)
	REFERENCES store(store_id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
GO
ALTER TABLE inventory
    ADD CONSTRAINT fk_inventory_film
	FOREIGN KEY(film_id)
	REFERENCES film(film_id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
GO
CREATE INDEX idx_fk_film_id USING BTREE 
    ON inventory(film_id)
GO
CREATE INDEX idx_store_id_film_id USING BTREE 
    ON inventory(store_id, film_id)
GO
CREATE TABLE language  ( 
    language_id	tinyint UNSIGNED AUTO_INCREMENT NOT NULL,
    name       	char(20) NOT NULL,
    last_update	timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(language_id) USING BTREE
)
ENGINE = InnoDB
CHARACTER SET utf8mb3
COLLATE utf8_general_ci
AUTO_INCREMENT = 6
GO
CREATE TABLE payment  ( 
    payment_id  	smallint UNSIGNED AUTO_INCREMENT NOT NULL,
    customer_id 	smallint UNSIGNED NOT NULL,
    staff_id    	tinyint UNSIGNED NOT NULL,
    rental_id   	int NULL,
    amount      	decimal(5,2) NOT NULL,
    payment_date	datetime NOT NULL,
    last_update 	timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(payment_id) USING BTREE
)
ENGINE = InnoDB
CHARACTER SET utf8mb3
COLLATE utf8_general_ci
AUTO_INCREMENT = 16049
GO
ALTER TABLE payment
    ADD CONSTRAINT fk_payment_staff
	FOREIGN KEY(staff_id)
	REFERENCES staff(staff_id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
GO
ALTER TABLE payment
    ADD CONSTRAINT fk_payment_rental
	FOREIGN KEY(rental_id)
	REFERENCES rental(rental_id)
	ON DELETE SET NULL
	ON UPDATE CASCADE
GO
ALTER TABLE payment
    ADD CONSTRAINT fk_payment_customer
	FOREIGN KEY(customer_id)
	REFERENCES customer(customer_id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
GO
CREATE TRIGGER payment_date 
    BEFORE INSERT ON payment
    FOR EACH ROW
    SET NEW.payment_date = NOW()
GO
CREATE INDEX fk_payment_rental USING BTREE 
    ON payment(rental_id)
GO
CREATE INDEX idx_fk_customer_id USING BTREE 
    ON payment(customer_id)
GO
CREATE INDEX idx_fk_staff_id USING BTREE 
    ON payment(staff_id)
GO
CREATE TABLE rental  ( 
    rental_id   	int AUTO_INCREMENT NOT NULL,
    rental_date 	datetime NOT NULL,
    inventory_id	mediumint UNSIGNED NOT NULL,
    customer_id 	smallint UNSIGNED NOT NULL,
    return_date 	datetime NULL,
    staff_id    	tinyint UNSIGNED NOT NULL,
    last_update 	timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(rental_id) USING BTREE
)
ENGINE = InnoDB
CHARACTER SET utf8mb3
COLLATE utf8_general_ci
AUTO_INCREMENT = 16049
GO
ALTER TABLE rental
    ADD CONSTRAINT rental_date
	UNIQUE (rental_date, inventory_id, customer_id) USING BTREE
GO
ALTER TABLE rental
    ADD CONSTRAINT fk_rental_staff
	FOREIGN KEY(staff_id)
	REFERENCES staff(staff_id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
GO
ALTER TABLE rental
    ADD CONSTRAINT fk_rental_inventory
	FOREIGN KEY(inventory_id)
	REFERENCES inventory(inventory_id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
GO
ALTER TABLE rental
    ADD CONSTRAINT fk_rental_customer
	FOREIGN KEY(customer_id)
	REFERENCES customer(customer_id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
GO
CREATE TRIGGER rental_date 
    BEFORE INSERT ON rental
    FOR EACH ROW
    SET NEW.rental_date = NOW()
GO
CREATE INDEX idx_fk_customer_id USING BTREE 
    ON rental(customer_id)
GO
CREATE INDEX idx_fk_inventory_id USING BTREE 
    ON rental(inventory_id)
GO
CREATE INDEX idx_fk_staff_id USING BTREE 
    ON rental(staff_id)
GO
CREATE TABLE staff  ( 
    staff_id   	tinyint UNSIGNED AUTO_INCREMENT NOT NULL,
    first_name 	varchar(45) NOT NULL,
    last_name  	varchar(45) NOT NULL,
    address_id 	smallint UNSIGNED NOT NULL,
    picture    	blob NULL,
    email      	varchar(50) NULL,
    store_id   	tinyint UNSIGNED NOT NULL,
    active     	tinyint(1) NOT NULL DEFAULT '1',
    username   	varchar(16) NOT NULL,
    password   	varchar(40) COLLATE utf8_bin NULL,
    last_update	timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(staff_id) USING BTREE
)
ENGINE = InnoDB
CHARACTER SET utf8mb3
COLLATE utf8_general_ci
AUTO_INCREMENT = 2
GO
ALTER TABLE staff
    ADD CONSTRAINT fk_staff_store
	FOREIGN KEY(store_id)
	REFERENCES store(store_id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
GO
ALTER TABLE staff
    ADD CONSTRAINT fk_staff_address
	FOREIGN KEY(address_id)
	REFERENCES address(address_id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
GO
CREATE INDEX idx_fk_address_id USING BTREE 
    ON staff(address_id)
GO
CREATE INDEX idx_fk_store_id USING BTREE 
    ON staff(store_id)
GO
CREATE TABLE store  ( 
    store_id        	tinyint UNSIGNED AUTO_INCREMENT NOT NULL,
    manager_staff_id	tinyint UNSIGNED NOT NULL,
    address_id      	smallint UNSIGNED NOT NULL,
    last_update     	timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(store_id) USING BTREE
)
ENGINE = InnoDB
CHARACTER SET utf8mb3
COLLATE utf8_general_ci
AUTO_INCREMENT = 2
GO
ALTER TABLE store
    ADD CONSTRAINT idx_unique_manager
	UNIQUE (manager_staff_id) USING BTREE
GO
ALTER TABLE store
    ADD CONSTRAINT fk_store_staff
	FOREIGN KEY(manager_staff_id)
	REFERENCES staff(staff_id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
GO
ALTER TABLE store
    ADD CONSTRAINT fk_store_address
	FOREIGN KEY(address_id)
	REFERENCES address(address_id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
GO
CREATE INDEX idx_fk_address_id USING BTREE 
    ON store(address_id)
GO
